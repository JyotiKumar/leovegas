package com.leo.vegas.service.impl;

import com.google.common.base.Stopwatch;
import com.leo.vegas.entity.WalletAccount;
import com.leo.vegas.entity.WalletTransaction;
import com.leo.vegas.exception.ApiException;
import com.leo.vegas.model.TransactionModel;
import com.leo.vegas.repository.WalletAccountRepository;
import com.leo.vegas.repository.WalletTransactionRepository;
import com.leo.vegas.service.WalletTransactionService;
import com.leo.vegas.util.TransactionStatus;
import com.leo.vegas.util.TransactionType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.leo.vegas.exception.ApiErrorMessageAndCode.TRANSACTION_ID_EXISTS;
import static com.leo.vegas.exception.ApiErrorMessageAndCode.invalidTransactionId;

@Service
@Slf4j
public class WalletTransactionServiceImpl implements WalletTransactionService {

	private final WalletTransactionRepository walletTransactionRepository;
	private final WalletAccountRepository walletAccountRepository;
	private final WalletAccountServiceImpl walletAccountServiceImpl;
	@PersistenceContext(unitName = "entityManagerFactory")
	private EntityManager entityManager;
	private final Lock lock = new ReentrantLock();
	private final Semaphore semaphore = new Semaphore(1);




	@Autowired
	public WalletTransactionServiceImpl(WalletTransactionRepository walletTransactionRepository,
	                                    WalletAccountRepository walletAccountRepository,
	                                    WalletAccountServiceImpl walletAccountServiceImpl, EntityManager entityManager) {
		this.walletTransactionRepository = walletTransactionRepository;
		this.walletAccountRepository = walletAccountRepository;
		this.walletAccountServiceImpl = walletAccountServiceImpl;
		this.entityManager = entityManager;
	}


	@Override
	//@Transactional
	public  WalletTransaction saveTransaction(TransactionModel transactionModel) {

		WalletTransaction transaction=null;
		Stopwatch stopwatch = Stopwatch.createStarted();
		TransactionSynchronizationManager.initSynchronization();
		entityManager.clear();
		lock.lock();
		try {
		//	semaphore.acquire();
			WalletAccount walletAccount = walletAccountRepository.findByUserId(transactionModel.getUserId());
			//entityManager.lock(walletAccount, LockModeType.OPTIMISTIC_FORCE_INCREMENT);
			log.info("Thread name entered {}", Thread.currentThread().getName());
			WalletAccount updateWalletAccount =null;


			boolean transactionIdExists = walletTransactionRepository.existsByTransactionId(transactionModel.getTransactionId());
			if (transactionIdExists) {
				log.warn("Transaction id already exists {}", transactionModel.getTransactionId());
				throw new ApiException(invalidTransactionId, TRANSACTION_ID_EXISTS);
			}
			transaction = new WalletTransaction();
			transaction.setTransactionAmount(transactionModel.getAmount().abs());
			transaction.setWalletAccount(null);
			transaction.setTransactionStatus(TransactionStatus.INITIATED);
			transaction.setTransactionType(transactionModel.getTransactionType());
			transaction.setTransactionId(transactionModel.getTransactionId());
			WalletTransaction walletTransaction = walletTransactionRepository.save(transaction);

			if (TransactionType.DEBIT.equals(transactionModel.getTransactionType())) {
				updateWalletAccount = walletAccountServiceImpl.debitWalletAccount(walletAccount.getId(), transactionModel.getAmount(), transactionModel.getTransactionId());


				//entityManager.flush();
			} else if (TransactionType.CREDIT.equals(transactionModel.getTransactionType())) {
				updateWalletAccount = walletAccountServiceImpl.creditWalletAccount(walletAccount.getUserId(), transactionModel.getAmount(), transactionModel.getTransactionId());
					//entityManager.refresh(walletAccount);

			}

			walletTransaction.setTransactionStatus(TransactionStatus.COMPLETED);
			walletTransaction.setWalletAccount(updateWalletAccount);
			walletTransactionRepository.save(walletTransaction);
			entityManager.detach(walletAccount);
			log.info("Total execution time {} in ms", stopwatch.elapsed(TimeUnit.MILLISECONDS));
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			lock.unlock();
			//semaphore.release();
		}
		log.info("Thread name exited {}", Thread.currentThread().getName());
		return transaction;
	}

	@Override
	public List<WalletTransaction> findTransactionsByUserId(String userId) {
		return walletTransactionRepository.findByWalletAccountUserId(userId);
	}
}
