package com.leo.vegas.service.impl;

import com.leo.vegas.entity.WalletAccount;
import com.leo.vegas.exception.ApiException;
import com.leo.vegas.repository.WalletAccountRepository;
import com.leo.vegas.service.WalletAccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.leo.vegas.exception.ApiErrorMessageAndCode.BALANCE_ERROR_001;
import static com.leo.vegas.exception.ApiErrorMessageAndCode.insufficientBalance;

/**
 * Service that gives helpful method to interact with wallet account.
 */
@Service
@Slf4j
public class WalletAccountServiceImpl implements WalletAccountService {

	private final WalletAccountRepository walletAccountRepository;
	private final EntityManager entityManager;
	private final Lock lock = new ReentrantLock();

	@Autowired
	public WalletAccountServiceImpl(WalletAccountRepository walletAccountRepository, EntityManager entityManager) {
		this.walletAccountRepository = walletAccountRepository;
		this.entityManager = entityManager;
	}

	@Override
	@Transactional(propagation = Propagation.MANDATORY)
	public WalletAccount debitWalletAccount(Long accountId, BigDecimal amount, String transactionId) {

		Optional<WalletAccount> walletAccount = walletAccountRepository.findById(accountId);

		if (walletAccount.isPresent()) {

			WalletAccount beforeUpdate = walletAccount.get();

			BigDecimal afterDebitAmount = beforeUpdate.getAvailableBalance().subtract(amount);
			if (afterDebitAmount.compareTo(BigDecimal.ZERO) >= 0) {
				BigDecimal updatedBalance = beforeUpdate.getAvailableBalance().subtract(amount);
				beforeUpdate.setAvailableBalance(updatedBalance);
				return walletAccountRepository.save(beforeUpdate);

			} else {
				log.warn("Insufficient fund to make transaction for transaction id {}", transactionId);
				throw new ApiException(insufficientBalance, BALANCE_ERROR_001);
			}
		}

		return new WalletAccount();
	}


	@Override
	public WalletAccount creditWalletAccount(String userId, final BigDecimal amount, final String transactionId) {

		WalletAccount walletAccount = new WalletAccount();
		log.info("Thread name {}", Thread.currentThread().getName());


		WalletAccount beforeUpdate = walletAccountRepository.findByUserId(userId);

		log.warn("version id {}", beforeUpdate.getVersionId());
		log.info("Before Account balance updated to {}  for transaction id {} thread name {}", beforeUpdate.getAvailableBalance(),
					transactionId, Thread.currentThread().getName());
		BigDecimal updatedBalance = beforeUpdate.getAvailableBalance().add(amount);

		log.info("updatedBalance ================ {}", updatedBalance);
		beforeUpdate.setAvailableBalance(updatedBalance);


		walletAccount = walletAccountRepository.save(beforeUpdate);
		log.warn("version id {}", walletAccount.getVersionId());

		//entityManager.detach(beforeUpdate);
		//entityManager.merge(walletAccount);
		//log.info("Account balance before update {} and after updated to {}  for transaction id {}", beforeUpdate.getAvailableBalance(),updateWalletAccount.getAvailableBalance(), transactionId);
		//	Assert.isTrue(updateWalletAccount.getAvailableBalance()
		//			.compareTo(beforeUpdate.getAvailableBalance()) ==  0, "not updated");
		//TimeUnit.MILLISECONDS.sle>ep(100);
		return walletAccount;
	}

	@Override
	public WalletAccount findByUserId(String userId) {
		return walletAccountRepository.findByUserId(userId);
	}
}
